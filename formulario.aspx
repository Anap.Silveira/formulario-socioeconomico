﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="formulario.aspx.vb" Inherits="Formulario_Socio_Economico.formulario" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">
        #form1 {
            height: 1412px;
        }
    </style>
</head>
<body style="height: 1405px; width: 795px">
    <form id="form1" runat="server">
        <div>
            <asp:Label ID="cabecalho" runat="server" BackColor="#CCFFFF" BorderColor="Black" Font-Bold="True" Font-Names="Arial" Font-Size="Larger" Font-Strikeout="False" Text="Pesquisa Socioeconômica" Width="800px"></asp:Label>
        </div>
        <p>
            <asp:Label ID="LbNome" runat="server" Font-Bold="True" Font-Names="Arial" Text="Nome:"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:TextBox ID="TbNome" runat="server" CausesValidation="True" Height="16px" MaxLength="60" Width="518px"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RfvNome" runat="server" ControlToValidate="TbNome" EnableClientScript="False" ErrorMessage="*Nome obrigátorio" Font-Bold="False" Font-Size="Small" ForeColor="#FF3300"></asp:RequiredFieldValidator>
        </p>
        <p>
            <asp:Label ID="LbTelefone" runat="server" Font-Bold="True" Font-Names="Arial" Text="Telefone: "></asp:Label>
&nbsp;
            <asp:TextBox ID="Tbtelefone" runat="server" CausesValidation="True" MaxLength="11" Width="234px"></asp:TextBox>
        </p>
        <p>
            <asp:Label ID="LbEndereço" runat="server" Font-Bold="True" Font-Names="Arial" Text="Endereço:"></asp:Label>
&nbsp;<asp:TextBox ID="TbEndereço" runat="server" CausesValidation="True" Width="273px"></asp:TextBox>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Label ID="LbBairro" runat="server" Font-Bold="True" Font-Names="Arial" Text="Bairro: "></asp:Label>
            <asp:TextBox ID="TbBairro" runat="server"></asp:TextBox>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:RequiredFieldValidator ID="RfvEndereco" runat="server" ControlToValidate="TbEndereço" EnableClientScript="False" ErrorMessage="*Endereço obrigatório" Font-Names="Arial" Font-Size="Small" ForeColor="#FF3300"></asp:RequiredFieldValidator>
        </p>
        <p>
            <asp:Label ID="lbEmail" runat="server" Font-Bold="True" Font-Names="Arial" Text="E-mail:"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:TextBox ID="TbEmail" runat="server" CausesValidation="True" Width="531px"></asp:TextBox>
            <asp:RegularExpressionValidator ID="RevEmail" runat="server" ControlToValidate="TbEmail" EnableClientScript="False" ErrorMessage="*E-mail inválido" Font-Bold="False" Font-Size="Small" ForeColor="#FF3300" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
        </p>
        <p>
            <asp:Label ID="LbDtNascimento" runat="server" Font-Bold="True" Font-Names="Arial" Text="Data de Nascimento:"></asp:Label>
            <asp:TextBox ID="TbNascimento" runat="server" CausesValidation="True" Width="178px"></asp:TextBox>
            <asp:CustomValidator ID="CvNascimento" runat="server" ControlToValidate="TbNascimento" EnableClientScript="False" ErrorMessage="*Data de nascimento inválida" Font-Size="Small" ForeColor="#FF3300"></asp:CustomValidator>
        </p>
        <p>
            <asp:Label ID="LbQuestao1" runat="server" Font-Bold="True" Font-Names="Arial" Text="Quantas pessoas reside na sua casa?"></asp:Label>
&nbsp;<asp:TextBox ID="TbQuestao01" runat="server" CausesValidation="True" Height="22px" Width="93px"></asp:TextBox>
        </p>
        <p>
            <asp:Label ID="LbQuestao02" runat="server" Font-Bold="True" Font-Names="Arial" Text="Qual a média salarial da sua família?"></asp:Label>
&nbsp;<asp:RadioButtonList ID="RblistQuestao02" runat="server" CausesValidation="True" Font-Names="Arial">
                <asp:ListItem Value="De 0 a 1.100"></asp:ListItem>
                <asp:ListItem Value="De 1.101 a 2.500"></asp:ListItem>
                <asp:ListItem Value="De 2.501 a 5.000"></asp:ListItem>
                <asp:ListItem>Mais de 5.001</asp:ListItem>
            </asp:RadioButtonList>
        </p>
        <p>
            <asp:Label ID="LbQuestao03" runat="server" Font-Bold="True" Font-Names="Arial" Text="Qual a sua escolaridade?"></asp:Label>
&nbsp;
            <asp:DropDownList ID="DdlistQuestao03" runat="server">
                <asp:ListItem>Selecione...</asp:ListItem>
                <asp:ListItem>Fundamental incompleto</asp:ListItem>
                <asp:ListItem>Fundamental completo</asp:ListItem>
                <asp:ListItem>Ensino medio incompleto</asp:ListItem>
                <asp:ListItem>Ensino medio completo</asp:ListItem>
                <asp:ListItem>Superior cursando</asp:ListItem>
                <asp:ListItem>Superior completo</asp:ListItem>
            </asp:DropDownList>
        </p>
        <p>
            <asp:Label ID="LbQuestao04" runat="server" Font-Bold="True" Font-Names="Arial" Text="Situação da moradia: "></asp:Label>
&nbsp;<asp:RadioButtonList ID="RblistQuestao04" runat="server" Font-Names="Arial">
                <asp:ListItem>Imóvel proprío</asp:ListItem>
                <asp:ListItem>Imóvel alugado</asp:ListItem>
                <asp:ListItem>Imóvel financiado</asp:ListItem>
            </asp:RadioButtonList>
        </p>
        <p>
            <asp:Label ID="LbQuestao05" runat="server" Font-Bold="True" Font-Names="Arial" Text="Você é casado ou amasiado? "></asp:Label>
            <asp:DropDownList ID="DdlistQuestao05" runat="server">
                <asp:ListItem>Selecione...</asp:ListItem>
                <asp:ListItem>Sim</asp:ListItem>
                <asp:ListItem>Não</asp:ListItem>
            </asp:DropDownList>
            &nbsp;</p>
        <p style="height: 6px">
&nbsp;<asp:Label ID="LbQuestao06" runat="server" Font-Bold="True" Font-Names="Arial" Text="De acordo com as categorias de cor/raça do IBGE, você se declara?"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        </p>
        <asp:RadioButtonList ID="RblistQuestao06" runat="server" style="margin-top: 0px">
            <asp:ListItem>Branco</asp:ListItem>
            <asp:ListItem>Preto</asp:ListItem>
            <asp:ListItem>Amarelo (de origem asiática)</asp:ListItem>
            <asp:ListItem>Pardo</asp:ListItem>
            <asp:ListItem>indígena</asp:ListItem>
        </asp:RadioButtonList>
        <p>
            <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Names="Arial" Text="Escolha dos ítens abaixo o que você possuem em casa:"></asp:Label>
            <asp:CheckBoxList ID="CblistQuestao06" runat="server">
                <asp:ListItem>Televisor</asp:ListItem>
                <asp:ListItem>Computador de mesa</asp:ListItem>
                <asp:ListItem>SmartPhone</asp:ListItem>
                <asp:ListItem>Lavadora de Roupa</asp:ListItem>
                <asp:ListItem>Secadora de Roupa</asp:ListItem>
                <asp:ListItem>Video Game</asp:ListItem>
                <asp:ListItem>Lavadoura de pratos</asp:ListItem>
                <asp:ListItem>Microondas</asp:ListItem>
                <asp:ListItem>Forno elétrico</asp:ListItem>
                <asp:ListItem>Notebook</asp:ListItem>
            </asp:CheckBoxList>
        </p>
        <p>
            &nbsp;</p>
        <p>
            &nbsp;</p>
        <p>
            &nbsp;</p>
        <p>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Button ID="BtEnviar" runat="server" BackColor="#CCFFFF" Font-Bold="True" Height="31px" OnClientClick="Formulário enviado. Obrigada!" Text="Enviar" Width="80px" />
        </p>
    </form>
</body>
</html>
