﻿'------------------------------------------------------------------------------
' <gerado automaticamente>
'     Este código foi gerado por uma ferramenta.
'
'     As alterações ao arquivo poderão causar comportamento incorreto e serão perdidas se
'     o código for recriado
' </gerado automaticamente>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class formulario

    '''<summary>
    '''Controle form1.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents form1 As Global.System.Web.UI.HtmlControls.HtmlForm

    '''<summary>
    '''Controle cabecalho.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents cabecalho As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle LbNome.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents LbNome As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle TbNome.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents TbNome As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Controle RfvNome.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents RfvNome As Global.System.Web.UI.WebControls.RequiredFieldValidator

    '''<summary>
    '''Controle LbTelefone.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents LbTelefone As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle Tbtelefone.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents Tbtelefone As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Controle LbEndereço.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents LbEndereço As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle TbEndereço.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents TbEndereço As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Controle LbBairro.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents LbBairro As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle TbBairro.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents TbBairro As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Controle RfvEndereco.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents RfvEndereco As Global.System.Web.UI.WebControls.RequiredFieldValidator

    '''<summary>
    '''Controle lbEmail.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lbEmail As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle TbEmail.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents TbEmail As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Controle RevEmail.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents RevEmail As Global.System.Web.UI.WebControls.RegularExpressionValidator

    '''<summary>
    '''Controle LbDtNascimento.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents LbDtNascimento As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle TbNascimento.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents TbNascimento As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Controle CvNascimento.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents CvNascimento As Global.System.Web.UI.WebControls.CustomValidator

    '''<summary>
    '''Controle LbQuestao1.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents LbQuestao1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle TbQuestao01.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents TbQuestao01 As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Controle LbQuestao02.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents LbQuestao02 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle RblistQuestao02.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents RblistQuestao02 As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''Controle LbQuestao03.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents LbQuestao03 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle DdlistQuestao03.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents DdlistQuestao03 As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Controle LbQuestao04.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents LbQuestao04 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle RblistQuestao04.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents RblistQuestao04 As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''Controle LbQuestao05.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents LbQuestao05 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle DdlistQuestao05.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents DdlistQuestao05 As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Controle LbQuestao06.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents LbQuestao06 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle RblistQuestao06.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents RblistQuestao06 As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''Controle Label1.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents Label1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle CblistQuestao06.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents CblistQuestao06 As Global.System.Web.UI.WebControls.CheckBoxList

    '''<summary>
    '''Controle BtEnviar.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents BtEnviar As Global.System.Web.UI.WebControls.Button
End Class
